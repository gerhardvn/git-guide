# Git Guide

A Guide to more than the basics of git usage.

Build with [Jekyll](https://jekyllrb.com/)
* `bundle exec jekyll build` - One off build.
* `bundle exec jekyll serve` - Serve to test.

Theme is [Solo](http://chibicode.github.io/solo/)

Hosted [here at Gitlab Pages](https://gerhardvn.gitlab.io/git-guide/)
