---
layout: default
---

This is a reference guide for common [git](https://git-scm.com/) commands. 

## Clone
Clone from repo:  
`git clone http://user@bitbucket.xxx.com:7990/yyy/zzz/instrument.git`

Clone from repo to a folder:    
`git clone http://user@bitbucket.xxx.com:7990/yyy/zzz/instrument.git folder`

Retrieve the most recent changes and merge if current branch changed:  
`git pull`

Retrieve the most recent changes but do not apply:   
`git fetch`


----------------------------
## Checkout
Checkout a branch for editing:  
`git checkout XXX-123-nonblocking-calls`

Revert file two revisions:   
`git checkout master~2 file`

Checkout while merging local changes:  
`git checkout -m XXX-123-nonblocking-calls`

----------------------------
## Add
Stage changes

Add all changes  
`git add .`

Track a new File:  
`git add -N new_file`

Add in interactive mode:  
`git add -i`

Remove a file and stop tracking:  
`git rm (file)`

----------------------------
## Commit
Save staged changes to Repository

Commit changes to your repository:  
`git commit`

Commit changes to your repository with auto add/remove:  
`git commit -a`

Commit only single file (with multiple changes added):  
`git commit Makefile`

Commit with message, avoid editor:  
`git commit -m "My message"`

----------------------------
## Push
Push local repository changes to remote repository.

Push changes   
`git push`

Push changes to another branch.   
`git push origin feature`  
An example of this is modified master but need to push to branch:      
`git push origin master:XXX-123-deployment`

----------------------------
## Merge
Merge changes from a branch into currently checked out code.  

`git merge origin/1.7.0`  
`git merge XXX-123-deployment`

Merge changes from a local branch into currently checked out code.  

`git checkout develop`  
`git merge master`

----------------------------
## Tag

List tags
* `git tag`
* `git tag -l "v1.*"`

Tag

`git tag v1.1`

Delete tag

`git tag -d v1.1`

----------------------------
## Branch

List branches (the asterisk denotes the current branch)  
`git branch`                    
List all branches (local and remote)  
`git branch -a`                 
Create a new branch   
`git branch XXX-123-branch_name`  
Delete a branch   
`git branch -d XXX-123-branch_name`   

------------------------------
## Rebase your branch on master:  
This will get changes to master since you branched. In effect move the root of your branch forward:
  
`git checkout XXX-123-deployment`  
`git rebase master`

-----------------------
## Compare repositories
`git diff <local branch> <remote>/<remote branch>`  
`git diff origin` is sufficient if you just compare with your upstream branch.  

----------------------------
## Config

Make changes to Git configuration

I don't speak Linux:   
`git config --global core.editor "'C:/Program Files (x86)/Notepad++/notepad++.exe' -multiInst -nosession"`
`git config --global core.editor "'C:/Program Files/Notepad++/notepad++.exe' -multiInst -nosession"`

Stop complaints of inconsistent line endings
`git config --global core.autocrlf true`

Change personal info:   
* `git config --global user.name "Joe Blow"`
* `git config --global user.email joe.blow@mail.com`

Use Proxy (All git action go through proxy):  
* `git config --global http.proxy http://DOMAIN\proxyUsername:proxyPassword@proxy.server.com:port`
* `git config --global http.proxy http://proxyUsername:proxyPassword@proxy.server.com:port`
* `git config --global http.proxy http://proxyUsername@proxy.server.com:port`

Or use a proxy for a specific domain (Direct access everything else):
* `git config --global http.https://gitlab.com.proxy http://username:password@10.1.1.10:8080`   
* `git config --global http.https://gitlab.com.proxy http://DOMAIN\username:password@10.1.1.10:8080`   
* Special character can use % encoding ex @ is %40
* `git config --global http.https://gitlab.com.sslVerify false`  

Remove these proxy settings:
* `git config --global --unset https.proxy`
* `git config --global --unset http.proxy`

Edit config files:
* `git config --global --edit`
* `git config --system --edit`
* `git config --local --edit`

File Location of config files:
`git config --list --show-origin`

----------------------------
## Discard changes

Discard all local changes to all files permanently:  
`git reset --hard`

Discard changes to one file:  
`git checkout -- file`

Undo a commit from some time in the past:  
find the commit you need to undo - `git log`  
create a new commit that undoes that commit  - `git revert [hash]`  

------------------------------------
{:refdef: style="text-align: center;"}
![public domain](CC_Public_Domain.png) [Source](https://gitlab.com/gerhardvn/git-guide)
{: refdef}
